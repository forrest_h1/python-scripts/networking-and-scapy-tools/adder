#!/usr/bin/python3
#Written by Forrest Hooker, 02/28/2023
#This is a simple single-host port scan written in Python. 

import sys
#Used for the actual port scanning
import socket
#Used for simple ICMP ping 
from pythonping import ping 
#Used to validate if user argument is a valid IP Address
import ipaddress
#Used for date recording
from datetime import datetime
#Used for sudo test in __argCheck__
import os 

## Vars/Arrs ##

#List of possible flags user would use to invoke __usage__ function
helpFlags = [
        "help",
        "HELP",
        "Help",
        "usage",
        "USAGE",
        "Usage",
        "--usage",
        "--Usage",
        "--USAGE",
        "-h",
        "-H",
        "--h",
        "--H",
] #End helpFlags list.


#List of ports this tool can identify (Will later be expanded)
knownPorts = {
        22: "SSH",
        23: "Telnet",
        25: "SMTP",
        49: "TACACS Login",
        53: "DNS",
        80: "HTTP",
        161: "SNMP",
        162: "SNMP Trap",
        177: "XDMCP",
        194: "IRC",
        280: "HTTP Mgmt.",
        443: "HTTP/S",
        554: "RTSP",
        #Both VMWare ports might be wrong
        902: "VMWare ESXI",
        903: "VMWare ESXI Server Agent",
        992: "Telnet over TLS/SSL",
        338: "Remote Desktop Protocol",
        4444: "I2P HTTP/S Proxy",
        5900: "VNC Server",
        6516: "Windows Admin Server",
        8000: "Python/Django Server",
        8006: "ProxMox Admin Web Interface",
        8008: "Alternate HTTP (Sometimes IBM)",
        8080: "Alternate HTTP (Apache, JIRA, Asterisk",
        8090: "Confluence",
        8123: "Home Assistant",
        8443: "Apache Tomcat SSL",
        8767: "TeamSpeak Voice Channel",
        9151: "Tor Browser",
        10500: "Zabbix Agent",
        10051: "Zabbix Client Trap",
        32764: "Backdoor for certain network devices"
} #End knownPorts Arr



#Literally me being lazy and using the same Bash printf codes I've used for years without grabbing a pip module  
class color: 
    #Call this at end of formatted text so colours don't spill everywhere
    RESET = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    #Apparently this changes depending on the OS? Some show as '[92m', never seen that before.
    GREEN = '\033[32m'
    RED = '\033[31m'
    YELLOW = '\033[33m'
    WHITE = '\033[37m'
    #Combo codes just so I'm not filling the screen with color codes lmao
    B_GREEN='\033[1m\033[32m'
    B_RED='\033[1m\033[31m'
    B_YELLOW='\033[1m\033[33m'
    B_WHITE='\033[1m\033[37m'
    #Python requires unicode instead of utf8, neat.
    _CHECK_ = '\u2713'


## Functions ##


#Main
def __Main__():
    #Function to check user's arguments
    __argCheck__()
    #Function to test if host is reachable
    __pingTest__()
    #Function that actually scans for ports
    __portScan__()


#Function to parse user argument
def __argCheck__():
    #If script is not being run as root, then...
    if os.geteuid() != 0:
        #Notify user that this script needs root
        print(color.B_RED,"\nThis script needs sudo access to properly work due to the pythonping module\n",color.RESET)
        #Exit script
        sys.exit()
    #Else, if no argument is given by the user, then...
    elif not len(sys.argv) == 2:
        #Notify user proper address format
        print(color.RED,"\nNo argument given. Please run this script with a valid IPv4 address as an argument.\n",color.RESET)
        #Print simple usage message
        print(color.YELLOW,"Ex: ./adder.py [x.x.x.x]\n\n",color.RESET)
        #Exit script
        sys.exit()
    #Else, if user argument matches any of the helpFlags elements, then...
    elif sys.argv[1] in helpFlags:
        __usage__()
    #Else, if argument MIGHT be an IP...
    else:
        #Globally define targetHost for other function calls
        global targetHost
        #Define targetHost as the user's argument
        targetHost = sys.argv[1]
        try:
            print("\n",color.BOLD,color.UNDERLINE,"Adder v1.1 - (https://gitlab.com/forrest_h1/python-scripts/networking-and-scapy-tools/adder)",color.RESET, sep='')
            #Try to validate the argument with the ipaddress module
            validTarget = ipaddress.ip_address(targetHost)
            #Why is python printing so weird, newlines create a space on the newline lol
            print("")
            #Notify user that host has a valid IP
            print(color.B_GREEN,targetHost," validated - ", color._CHECK_, color.RESET, sep='')
        #If ValueError raised...
        except ValueError:
            print("")
            #Notify user
            print(color.RED,targetHost," is not a valid IPv4 Address [x.x.x.x]\n",color.RESET, sep='')


#Function to ping the host before an attempt to open any socket is made
def __pingTest__():
    try: 
        #ping the target host with Verbose (subject to change) output
        ping(targetHost, verbose=False)
        #If up, notify that host is reachable and scan will continue
        print(color.B_GREEN,targetHost," is reachable - ",color._CHECK_,color.RESET, sep='', end="\n\n")
    #If a socket error is raised, then...
    except socket.error:
        #Notify user that host might be down.
        print(color.RED,targetHost," seems to be down. Check the address of the host.",color.RESET)
        #Exit script
        sys.exit()


#Function to scan each host by creating a TCP socket to scan for a port
def __portScan__():
    print("{}Starting scan for {}...\n{}".format(color.B_WHITE,targetHost,color.RESET))
    try:
        #For each conceivable port between 1 & 65535, do...
        for port in range(1,65535):
            #Notify the user of the port being scanned, overwriting the same line over and over
            print(color.WHITE,"Scanning port ", port,color.RESET, end='\r', sep='')
            #Define sock as a TCP socket that connects to the targetHost 
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            #Set socket's default timeout to 1 (Possibly make this user-defined?)
            socket.setdefaulttimeout(1)
            #Define the scan result as whether or not a connection to the port in range was successful or not
            scanResult = sock.connect_ex((targetHost,port))
            #If scanResult is successful, and the port happens to be in the knownPorts keys...
            if scanResult ==0 and port in knownPorts.keys():
                #Notify the user that not only was the port found, but the common usage is known for the targetHost
                print ('{}:{} - ({}){}{} is open on {}'.format(color.B_GREEN, port, knownPorts[port],color.RESET,color.GREEN, targetHost), sep='')
            #Else, if the scanResult is succesful but the port is NOT in the knownPorts keys...
            elif scanResult ==0:
                #Notify the user that a port unknown to this script has been found on the targetHost
                print ('{}:{} - {}(Unknown Port){}{} is open on {}'.format(color.B_GREEN,port,color.B_YELLOW,color.RESET,color.GREEN,targetHost))
            #Close the socket after each scan so no phantoms
            sock.close()
        #Long space because of '\r' line-overwrite
        print("                                          ")
        print(color.B_WHITE,"Scan completed.         \n\n",color.RESET, sep='')
    #Define exception for a CTRL+C
    except KeyboardInterrupt:
        print("                                   /r")
        #Notify user that script is exiting.
        print(color.YELLOW,"\nExiting.",color.RESET)
        #Close the TCP socket
        sock.close()
    #Define hostname resolution error (Don't know that this could actually occur in the middle of a scan, maybe if lease was dropped?)
    except socket.gaierror:
        #Notify user of problem
        print(color.B_RED,"\nWARNING! Possible DNS issue. Check your connection.",color.RESET)
        #Notify user of exit
        print(color.YELLOW,"\nExiting.",color.RESET)
        #Close the TCP socket
        sock.close()
    #Define socket error (Host is blocking or is down)
    except socket.error:
        #Notify user they might want to abandon the Coffee shop they've been at for 12 hours
        print(color.B_RED,"\nServer is no longer responding. Check your connection or GTFO of wherever you are",color.RESET)
        #Close the TCP socket
        sock.close()



def __usage__():
    print("Adder v1.1 - Single Host Port Scanner in Python (https://gitlab.com/forrest_h1/python-scripts/networking-and-scapy-tools/adder)")
    print("Usage: sudo ./adder.py [x.x.x.x] <- Target address to scan")
    print("NOTE: Adder only supports IPv4 addresses at this time.\n\n")
    sys.exit()


            
__Main__()
