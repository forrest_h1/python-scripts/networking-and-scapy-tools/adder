# Adder




Adder is a single-host port-scanner built as an improvement of my Perl port-scanner, Habiru (Formerly port_scan.pl). Rather than scan known ports, this tool is effectively given a defined range to scan and prints not only ports known to the tool, but unknown ports as well. The tool is built from the sockets, ipaddress, and pythonping packages. It's important to note that pythonping requires sudo access - I went with this method for a pure python ICMP ping rather than an os.system call.



Adder is named after the Puff Adder, and happens to share a name with my favorite mech from the Battletech universe :)




# Package requirements


-ipaddress - Used to validate a given IPv4 address (Working to expand this to IPv6)



-sockets - The package actually used to create a TCP socket for scans



-pythonping - Used to check if a host is up or down before scanning.


## Usage



Usage, in Adder's v1.1 stage, is very simple. Run `sudo ./adder.py [x.x.x.x]`, replacing `[x.x.x.x]` with a valid IPv4 scan. I will be extending the usage to allow for user-defined timeouts, custom port ranges/singular ports, and IPv6+Domain name scans.



## Todo



-Add IPv6 & Domain name support (v1.2)



-Add singular port scan function (v1.3)



-Potentially add method of checking host reachability without sudo access, as the only module that really needs this is pythonping for ICMP (v1.4?)



-Add custom defined port ranges, in addition to "Quick" scans that will only encompass ports known to the system that are commonly accesible through web or login sessions (v1.5, v1.6)





